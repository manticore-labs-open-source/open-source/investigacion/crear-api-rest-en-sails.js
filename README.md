# CREACIÓN DE API REST EN SAILS.JS

Una de las cosas que permite Sails.js, entre muchas otras, es el desarrollo de forma sencilla y rápida de un API REST. Por se considera como una herramienta muy importante del stack de software.

para ello instalamos sails.js  con el siguiente comando

![](./imagenes/Selección_112.png)

Para generar una nueva aplicación, solo nos cambiamos  al directorio donde desea que esté, y escribimos. Se mostrará un aviso para elegir la plantilla de proyecto.

![](./imagenes/Selección_113.png)

Escriba 1 para comenzar con nuestra plantilla de "Aplicación web": un proyecto de inicio con opinión que incluye funciones esenciales como inicio de sesión, recuperación de contraseña, correos electrónicos y facturación. O, si desea comenzar desde cero con un proyecto vacío, elija 2 para una aplicación Sails clásica. Una vez que haya elegido su plantilla, deberá esperar un momento para que se instalen algunas dependencias:

![](./imagenes/Selección_114.png)


Entonces, para verificar que la aplicación sea creado nos cambiamos al directorio creado: 

![](./imagenes/Selección_115.png)

ingrese al localhost en el puerto 1337

![](./imagenes/Selección_116.png)


para crear la API escribimos el siguiente comando:

![](./imagenes/Selección_117.png)

Con la herramienta POSTMAN, realizamos una consulta para verificar que el API rest de Estudiante se haya creado.

![](./imagenes/Selección_118.png)

Finalmente tenemos que definir las propiedades de nuestra entidad

![](./imagenes/Selección_119.png)

ingresamos un nuevo registro, mediante el metodo POST

![](./imagenes/Selección_120.png)

Y si nuevamente consultamos los estudiantes enviando una petición GET, obtenemos la siguiente información:

![](./imagenes/Selección_121.png)

ahora editamos los valores con el método PUT

![](./imagenes/Selección_122.png)

y también podemos eliminar un registro con el método DELETE

![](./imagenes/Selección_123.png)

si queremos volver a consultar este usuario, se mostrará un error.

![](./imagenes/Selección_124.png)

relaciones entre entidades con sails.js
Otra característica que Sails.js incorpora al generar nuevas APIs y que supondría bastante trabajo si tuviéramos que desarrollarlo manualmente es la gestión de relaciones entre entidades.

![](./imagenes/Selección_125.png)

Para explicar las relaciones entre entidades vamos a utilizar el ejemplo que utiliza el equipo de Sails.js en su propia documentación. El ejemplo que vamos a ver se basa en una relación uno a muchos, en el que las entidades van a ser Estudiante  y Materia y donde un estudiante va a poder tener un número indefinido de materias.

![](./imagenes/Selección_126.png)

En primer lugar, generamos nuestro API para la entidad Materia:

![](./imagenes/Selección_127.png)

Y la definimos sus propiedades tanto en el modelo de Estudiante como :en el Materia


![](./imagenes/Selección_128.png)


ingresamos registro de materias asociados a

![](./imagenes/Selección_129.png)

y ahora podemos a todos los estudiantes con sus respectivas materias

<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>



